import { Injectable } from '@angular/core';

@Injectable({
  providedIn: "root"
})
export class EquipoService {
  //informacion Leer mas... rdel [ nosotros component html ]

  equipo: any[] = [
    {
      nombre: "Alejandro",
      edad:"21",
      rol: "Alumno",
      especialidad:"wordpress",
      descripcion:"Su mision en la vida es trabajar con Wordpress tambien conocido como WPMan o el chico del meme lO SIENTOOO"
    },
    {
      nombre: "Carlos",
      edad: "30",
      rol: "Profesor",
      especialidad: "laravel",
      descripcion:"Conoce todos los lenguajes y tecnologias existentes pero sobre todo domina el framework Laravel de php es amante de ese lenguaje y lo defendera con uñas y dientes."
    },
    {
      nombre: "Tao Oscuro",
      edad: "20",
      rol: "Alumno",
      especialidad: "Dormir como un tronco",
      descripcion:"la reencarnacion de Snorlax en la vida real tiene el don de dormirse en el filo de una navaja ..."
    }
  ];

  constructor() {
    console.log("funcionando servicio");
  }

  obtenerEquipo() {
    return this.equipo;
  }

  obtenerUno(i) {
    return this.equipo[i];
  }
}



