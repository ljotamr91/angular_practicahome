import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.css"]
})
export class TableComponent implements OnInit {
  academia: Array<any> = [
    { rol: "Alumno", nombre: "Juan", edad: 29 },
    { rol: "Profesor", nombre: "Carlos", edad: 30 },
    { rol: "Alumno", nombre: "Jose", edad: 21 },
    { rol: "Jefe", nombre: "Lazar", edad: 50 },
    { rol: "Alumno", nombre: "Cristian", edad: 19 },
    { rol: "Profesor", nombre: "Tim", edad: 47 },
    { rol: "Alumno", nombre: "Sergio", edad: 25 },
    { rol: "Profesora", nombre: "Inma", edad: 32 },
    { rol: "Alumno", nombre: "Alejandro", edad: 16 },
    { rol: "Alumna", nombre: "Nieves", edad: 24 }
  ];

  constructor() {}

  ngOnInit() {}
}
