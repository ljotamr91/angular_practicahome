import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Route } from '@angular/router';

// #############################################################################
//Servicios

import{ EquipoService } from './equipo.service';


// #############################################################################

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { FooterComponent } from './footer/footer.component';
import { ContactoComponent } from './contacto/contacto.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { Error404Component } from './error404/error404.component';
import { EquipoComponent } from './equipo/equipo.component';


//estas son las rutas de cada vista
//tambien podemos poner diferentes rutas que lleven al mismo sitio en el caso de necesitarlo para el multiidioma como table y tabla
const routes: Route[] = [
  { path: "", component: HomeComponent, pathMatch: "full" }, //cuando no espezifique me redirecciona a la pagina de inicio
  { path: "home", component: HomeComponent },
  { path: "inicio", component: HomeComponent },
  { path: "index", component: HomeComponent },
  { path: "table", component: TableComponent },
  { path: "tabla", component: TableComponent },
  { path: "equipo/:id", component: EquipoComponent },
  { path: "contacto", component: ContactoComponent },
  { path: "contact", component: ContactoComponent },
  { path: "nosotros", component: NosotrosComponent },
  { path: "error404", component: Error404Component },
  { path: "**", redirectTo: "error404", pathMatch: "full" } //cuando busquemos algo que no existe nos redirecciona a una pagina error 404
];

// Este es el modulo principal de angular la aplicacion en si.
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableComponent,
    EncabezadoComponent,
    FooterComponent,
    ContactoComponent,
    NosotrosComponent,
    Error404Component,
    EquipoComponent,

  ],
  imports: [BrowserModule,
            FormsModule,
            HttpClientModule,
            AppRoutingModule,
            RouterModule.forRoot(routes)
  ],
  providers: [ //añadimos los servicios
    EquipoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
