# MyApp 

datos de la aplicacion

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.5.

## Development server 

datos del servidor

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


README es documentacion e informacion acerca del proyecto version configuracion etc ( block de notas ).

########

En este proyecto trabajaremos con la tecnologia Angular en si realizando ejemplos y diferentes ejercicios todo quedara registrado por Git ya que controla el versionado del codigo a medida que vallamos terminando de hacer cosas comentaremos e iremos borrando otras para poder trabajar mas comodo y si queremos ver codigo antiguo iremos al repositorio para verlo.

1 Curso Angular 6 (Fazt) (repetir el crud en un componente nuevo )

https://www.youtube.com/watch?v=AR1tLGQ7COs&t=3842s

https://www.youtube.com/watch?v=OrCdt865WOg&t=1726s

__________________________________________________________________

2 curso de angular Juan Bluuweb !

https://www.youtube.com/watch?v=mog8EKQX5HI&list=PLPl81lqbj-4JaLibWSbTVrYTyHDadppKq


3 video fundamentos TS

http://www.typescriptlang.org/

https://www.youtube.com/watch?v=qJxYoiFzIC0&list=PLPl81lqbj-4JaLibWSbTVrYTyHDadppKq&index=3


ionic es parecido a bootstrap para angular


4 Documentacion ofical Angular rutas

https://angular.io/tutorial/toh-pt5

5 Bluweb Servicios
 https://www.youtube.com/watch?v=2hdQ3KE7_As&list=PLPl81lqbj-4JaLibWSbTVrYTyHDadppKq&index=19

 Explicacion servicios en angular

en el momento en que dos o más componentes tengan que acceder a los mismos datos y hacer operaciones similares con ellos, que podrían obligarnos a repetir código. Para solucionar estas situaciones tenemos a los servicios.

https://desarrolloweb.com/articulos/servicios-angular.html

______________________________________________________________________________________

Atajo Crear componente 
CTRL + Mayús + P crea un terminal integrado en visual studio
y para crear el nuevo componente escribiremos ng generate component nombre-del-componente y nos creara un nuevo componente con sus archivos y codigo plantilla. 

Crear service ng generate service

crear carpeta podemos utilizar comandos de linux como mk dir NombreCarpeta

crear componente dentro de otro componente una opcion es con ls cd mover hasta la carpeta que queremos y luego crear el componente y otro opcion es -> ( ng create component NombreCarpetaExistente/NuevoComponente)

Podemos abrebiar a la hora de generar componentes escribiendo ng g c nombre-del-componente

Consejo Consolas
es recomendable abrir el proyecto desde el visual studio code y ejecutamos una consola para el servidor y otra para crear componentes.

Angular funciona con modulos formados por diferentes componentes al iniciar la aplicacion realmente estamos iniciando un modulo.

los archivos con extension ts son archivos typescript,  con lo que trabajaremos dentro de angular, y mas tarde los convertira en js. ( lo hara Angular automaticamente al compilar )

tsconfig es el archivo de configuracion para los archivos typescript.(lenguaje subconjunto de javascript) sin ese config ts no podria funcionar.

package.json esta relacionada con node_modules lista todas las dependencias y codigo que necesita angular para la configuracion de proyecto, cuando queramos añadir librerias lo agregaremos en este documento y se instalara automaticamente cuando el proyecto se tenga que generar, la parte superior dentro del documento donde esta los comandos -ng -start... son comandos que podemos ejecutar en angular cli.

package-lock.json es la configuracion de como trabajara node.js ( no hay que modificar nada de este archivo).

node_modules archivos que necesita angular para trabajar Angular depende de muchas bibliotecas/librerias y estas a su vez dependen de otras pues aqui es donde descarga y lista todas.

el Routing outlet es algo similar a ajax ya que angular  no carga paginas enteras, si no que carga bloques de codigo todo en la misma pagina dando sensacion de que cargamos otra pagina  y funciona siempre cargando pequeños componentes 

Routing -> ?¿?

Router -> ?¿?

servicios?¿?

para almacenar los scripts/ librerias como por ejemplo bootstrap jquery etc se guardan en ->  ?¿?

caso 1 para bootsrtap carga el cdn y los scripts en el index html

e2e esta carpeta es para testear nuestra app ( concepto para experimentados ). 

editorconfig es la configuracion de nuestro editor de texto.

gitignore este archivo es el encargado de ignorar lo que subiremos a Git para no estar cargando y descargando las dependencias todo el tiempo.

SRC

es la carpeta donde vamos a modificar todo nuestro codigo de la aplicacion dentro de app encontraremos index.html style.css y junto al js forman nuestra aplicacion.

index.html el contenido de nuestra aplicacion dentro del index esta dentro de otra etiqueta llamada app-root ( la raiz de nuestra aplicacion)

style.css es donde daremos estilo a nuestra aplicacion

el resto de archivos de fuera es para que angular pueda trabajar.

tsconfig.app.json, tsconfig.spec.json, tslint.json son archivos de configuracion typescript ,originalmente Angular fue desarrollado utilizando typescript y para utilizar angular tambien necesitamos typescript.

assets esta carpeta es donde meteremos las fuentes, imagenes, iconos etc(archivos estaticos).

enviroments controla en que entorno trabajamos produccion desarrollo etc tambien html css aunque el proyecto realmente esta dentro de SRC -> APP.

favicon.ico es el icono de la pestaña del explorador.

npm install -g typescript ( instala TS )

tsc -v ( ver la version de ts )

--------------------------------------------------------------------------
SRC/APP

cada vez que eliminemos un componente tenemos que ir a app module ts y eliminar su respectivo import y Module

aqui es donde esta nuestra aplicacion en realidad (titulo, imagenes, etc)

app.component. (HTML, CSS, TS). y tambien app.component.spec.ts (hay encontraremos el titulo)

app.module.ts nos permite iniciar nuestra aplicacion de angular, NgModules es el modulo principal de la aplicacion  ( la etiqueta root dentro del index es un modulo  
main.ts lee el modulo principal de la aplicacion de APP y lo carga en la etiqueta root del index.html.

[Hola mundo]
PrimerComponente dentro de Componente -> template el texto que hay dentro es texto que se vera reflejada en nuestra pagina del explorador.

{Como se hace}

1 Creamos una carpeta dentro de app donde ira nuestro primer componente

2 creamos los archivos primer.component.html, primer.component.css y primer.component.ts.

3 empezaremos a escribir en nuestro archivo "primer.component.ts" para crear un hola mundo, ( mirar comentarios del codigo ) en el siguiente orden (primer.component.ts, app.component.html, app.module.ts )

primer.component.ts -> styles: ['h1 {background: #000; color: #fff;}'] 
// el motivo de añadir estilo aqui y no al general espara configurar el estilo especifico de algun bloque de codigo y no del general de la pagina

-------------------------------------------------------------------------

Como Trabajar en Angular

index.html es el indice de la aplicacion, lo primero que arranca siempre, 
esta a su vez llama al tag -> app root , que viene siendo el app.component.ts que es el que llamara los componentes que generamos en su html para trabajar con los componentes como si fueran pequeños bloques de codigo.

El HTML base esta en enviroments index.html que es la que llamara a los componentes. 

En el app-compoente HTML configuraremos bloques de codigo encapsulandolos en clases para llamarlos mas tarde en el enviroments index.html

En el componente CSS escribiremos todos los estilos deseados

d-none es lo mismo que display none pero en bootstrap

d-block es lo mismo que display block en bootstrap

En el componente TS (javascript) iran todas las funciones donde crearemos funciones y bloques de codigo para luego instanciarlos en otras partes del codigo

Los archivos de espec son pruebas unitarias para sus archivos de origen. La convención para las aplicaciones Angular es tener un archivo .spec.ts para cada archivo .ts. Se ejecutan utilizando el marco de prueba javascript de Jasmine a través del corredor de tareas Karma cuando usa el ng test comando.

Tambien tener mucho cuidado por que Angular detecta mayusculas.

(Como Debugear en Angular)

F12 Console leer los errores directamente.

para crear el archivo de rutas escribiremos ( por defecto dentro de app ) -> ng generate module app-routing --flat --module=app

como buscar palabras en todo el proyecto  = Ctrl + Shift + F

pasar practica home a angular
